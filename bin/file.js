const path = require("path");
const fs = require("fs");
const {COPYFILE_EXCL} = fs.constants;
const swig = require("swig"); //前端模板引擎

const SRC_PATH = path.resolve(__dirname, "../src");
const CURRENT_PATH = path.resolve(__dirname, "");
const APP_PATH = path.resolve(__dirname, "../");
const SRC_CONFIG = path.resolve(__dirname, "../src/config");

const fileArr = ["download-bg.jpg", "H5.ico", "logo.png", "logo1.png", "image.jpg", "zhanhu.png"];

const hash = {quduo: "趣多", meigaomei: "美高梅", dongfanghui: "GIV国际"};

module.exports = {
  removeFile,
  copyFile,
  createFile,
  createConfigFile
};

async function createFile(platform) {
  const template = swig.compileFile(`${CURRENT_PATH}/index.html.tpl`);
  const tpl = template({platform_name: hash[platform]});
  let index = path.join(APP_PATH, `./index.html`);
  let result = fs.existsSync(index);

  if (result) {
    let stats = fs.statSync(index);
    if (stats.isFile()) {
      console.log("您要删除的index.html文件存在,已经成功的将其删除");
      fs.unlinkSync(index);
      fs.appendFileSync(index, tpl);
    } else if (stats.isDirectory()) {
      console.log("请检查文件类型,可能是一个文件夹");
    } else {
      console.log("未知文件类型")
    }
  } else {
    console.log("您要删除的index.html文件并不存在");
    fs.appendFileSync(index, tpl);
  }
}

async function createConfigFile(platform) {
  const template = swig.compileFile(`${CURRENT_PATH}/app.config.js.tpl`);
  let platform_prefix = "";

  if (hash[platform] === "GIV国际") {
    platform_prefix = "df_";
  }

  if (hash[platform] !== "美高梅") {
    open_valid_user = "open";
  } else {
    open_valid_user = "false";
  }

  const tpl = template({
    platform_name: hash[platform],
    platform_prefix: platform_prefix,
    open_valid_user: open_valid_user
  });

  let config_path = path.join(SRC_CONFIG, `./app.config.js`);
  let result = fs.existsSync(config_path);

  if (result) {
    let stats = fs.statSync(config_path);
    if (stats.isFile()) {
      console.log("您要删除的app.config.js文件存在,已经成功的将其删除");
      fs.unlinkSync(config_path);
      fs.appendFileSync(config_path, tpl);
    } else if (stats.isDirectory()) {
      console.log("请检查文件类型,可能是一个文件夹");
    } else {
      console.log("未知文件类型")
    }
  } else {
    console.log("您要删除的app.config.js文件并不存在");
    fs.appendFileSync(config_path, tpl);
  }
}

function getFileInfo(filePath) {
  return new Promise((resolve, reject) => {
    fs.stat(filePath, (err, stats) => {
      if (err) {
        reject(`获取文件信息失败${err}`);
      }
      if (stats && stats.isFile()) {
        resolve(stats);
      } else {
        reject("获取文件信息失败!");
      }
    });
  });
}

function removeFile() {
  fileArr.forEach(item => {
    let p = path.join(SRC_PATH, `./assets/base/${item}`);
    if (fs.existsSync(p)) {
      console.log(`文件${item}删除完毕`);
      return fs.unlinkSync(p)
    }
    console.log(`${p}不存在`);
  })
}

function copyFile(platform) {
  fileArr.forEach(fileName => {
    let from = path.join(SRC_PATH, `./assets/${platform}/${fileName}`);
    let to = path.join(SRC_PATH, `./assets/base/${fileName}`);
    if (fs.existsSync(from)) {
      //默认情况下，如果 dest 已经存在，则覆盖它。 返回 undefined。
      console.log(`${platform}的${fileName}文件复制完毕`);
      return fs.copyFileSync(from, to);
    }
    console.log(`目标源文件并不存在${fileName}`);
  });
}

function handleRemove(fileName, filePath) {
  const promise = new Promise((resolve, reject) => {
    fs.unlink(filePath, err => {
      if (err) {
        reject("删除文件失败", err);
      } else {
        resolve(`${fileName}文件已删除`);
      }
    });
  });
  return promise;
}

function handleCopy(fileName, platform) {
  const promise = new Promise((resolve, reject) => {
    fs.copyFile(path.join(SRC_PATH, `./assets/${platform}/${fileName}`), path.join(SRC_PATH, `./assets/base/${fileName}`), COPYFILE_EXCL, err => {
      if (err) {
        throw err;
        reject(err);
      } else {
        resolve(`${hash[platform]}的${fileName}文件复制完毕`);
      }
    });
  });
  return promise;
}

function create(filePath, data) {
  return new Promise((resolve, reject) => {
    fs.appendFile(filePath, data, function (err) {
      if (err) {
        reject("添加数据或者创建失败", err);
      }
      resolve("创建文件成功!");
    });
  });
}
