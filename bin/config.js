const path = require("path");
const ADMIN_USER = 'win';
const DEVICE = "m";
const localhost = "192.168.254.145";

//开发目录
const SRC_PATH = path.resolve(__dirname, '../src');
//脚本启动路径
const BIN_PATH = path.resolve(__dirname, '');
//当前项目路径
const PROJECT_PATH = path.resolve(__dirname, '../');
//打包文件目录
const DIST_PATH = path.resolve(__dirname, '../dist');
//配置需要更新的svn文件夹所在的目录
const DESKTOP_PATH = `C:\\Users\\${ADMIN_USER}\\Desktop`;
const CURRENT_PATH = path.resolve(__dirname, '');
const APP_PATH = path.resolve(__dirname, '../');
const SVN_QUDUO_URL = `svn://${localhost}/quduo/web.public/${DEVICE}`;
const SVN_MEIGAO_URL = `svn://${localhost}/meigao/web.public/${DEVICE}`;
const SVN_DONGFANG_URL = `svn://${localhost}/dongfang/web.public/${DEVICE}`;

module.exports = {
  SRC_PATH,
  BIN_PATH,
  PROJECT_PATH,
  DESKTOP_PATH,
  DIST_PATH,
  CURRENT_PATH,
  APP_PATH,
  SVN_QUDUO_URL,
  SVN_MEIGAO_URL,
  SVN_DONGFANG_URL
};
