import Vue from "vue";
import Router from "vue-router";
// 组件;
const Vlay = resolve => {
  import("../docs/index").then(module => {
    resolve(module);
  });
};
// 登陆
const Login = resolve => {
  import("components/login/login").then(module => {
    resolve(module);
  });
};
// 注册
const Reg = resolve => {
  import("components/reg/reg").then(module => {
    resolve(module);
  });
};
// 绑定手机号
const BindMobile = resolve => {
  import("components/reg/bindMobile").then(module => {
    resolve(module);
  });
};
//获取手机验证码
const GetCode = resolve => {
  import("components/reg/getCode").then(module => {
    resolve(module);
  });
};
// 注册条款
const Clause = resolve => {
  import("components/reg/clause").then(module => {
    resolve(module);
  });
};
// 忘记密码
const ForgotPwd = resolve => {
  import("components/reg/forgotPwd").then(module => {
    resolve(module);
  });
};


// 添加银行卡
const MyCard = resolve => {
  import("components/index/withdrawCash/myCard").then(module => {
    resolve(module);
  });
};
// 首页
const Main = resolve => {
  import("components/homes/home").then(module => {
    resolve(module);
  });
};

// 我的页面
const Personal = resolve => {
  import("components/personal/personal").then(module => {
    resolve(module);
  });
};
const ModifyBaseInfo= resolve => {
  import("components/personal/modifyBaseInfo").then(module => {
    resolve(module);
  });
};
// 个人信息
const PersonalDetails = resolve => {
  import("components/personal/personalDetails").then(module => {
    resolve(module);
  });
};

// 客服
const KeFu = resolve => {
  import("components/index/main/kefu").then(module => {
    resolve(module);
  });
};

// 活动页面
const Promotion = resolve => {
  import("components/index/main/promotion").then(module => {
    resolve(module);
  });
};
// 活动详情
const PromotionDetail = resolve => {
  import("components/index/main/promotionDetail/promotionDetail").then(
    module => {
      resolve(module);
    }
  );
};
// 签到页面
const Signin = resolve => {
  import("components/personal/signin").then(module => {
    resolve(module);
  });
};

// 用户设置
const UserSetting = resolve => {
  import("components/personal/userSetting").then(module => {
    resolve(module);
  });
};
// 投注记录
const Betting = resolve => {
  import("components/personal/betting").then(module => {
    resolve(module);
  });
};
// 投注记录
const Zhuihao = resolve => {
  import("components/personal/zhuihao").then(module => {
    resolve(module);
  });
};
// 我的空间
const MySpace = resolve => {
  import("components/personal/mySpace").then(module => {
    resolve(module);
  });
};
// 我的好友
const Friend = resolve => {
  import("components/personal/friend").then(module => {
    resolve(module);
  });
};
const InviteFriends = resolve => {
  import("components/personal/inviteFriends").then(module => {
    resolve(module);
  })
}
// 我的订单
const MyOrder = resolve => {
  import("components/personal/myOrder").then(module => {
    resolve(module);
  })
}
// 发货信息
const SendInfo = resolve => {
  import("components/personal/sendInfo").then(module => {
    resolve(module);
  })
}

// 充值详情
const Rechargebill = resolve => {
  import("components/personal/Rechargebill").then(module => {
    resolve(module);
  });
};

// 提现记录
const WithdrawRecord = resolve => {
  import("components/personal/withdrawRecord").then(module => {
    resolve(module);
  });
};
// 提现详情
const WithdrawDetail = resolve => {
  import("components/personal/withdrawDetail").then(module => {
    resolve(module);
  });
};
// 账变记录
const ZbRecord = resolve => {
  import("components/personal/zbRecord").then(module => {
    resolve(module);
  });
};
// 消息中心
const MsgCenter = resolve => {
  import("components/personal/msgCenter").then(module => {
    resolve(module);
  });
};
// 我的提醒
const Remind = resolve => {
  import("components/personal/remind").then(module => {
    resolve(module);
  });
};
// 我的收藏
const Collection = resolve => {
  import("components/personal/collection").then(module => {
    resolve(module);
  })
}
// 我的帖子
const Bbs = resolve => {
  import ("components/personal/bbs").then(module => {
    resolve(module);
  })
}

// 修改密码
const ModifyPwd = resolve => {
  import("components/personal/modifyPwd").then(module => {
    resolve(module);
  });
};
// 设置资金密码
const SetPwd = resolve => {
  import("components/personal/setPwd").then(module => {
    resolve(module);
  });
};

// 设置手机号码
// const BindMobile = resolve => {
//   import("components/personal/bindMobile").then(module => {
//     resolve(module);
//   });
// };


const Unknown = resolve => {
  import("components/404/404").then(module => {
    resolve(module);
  });
};

// 入驻合作 page
const SettledInfo = resolve => {
  import("components/settledCooperation/settledInfo").then(module => {
    resolve(module);
  });
};

// 评测中心
const Evaluating = resolve => {
  import("components/evaluating/evaluatingCenter").then(module => {
    resolve(module);
  });
}

// 娱乐城详情
const Entertainment = resolve => {
  import("components/entertainment/Entertainment").then(module => {
    resolve(module);
  });
}
// 认证中心
const Authent = resolve => {
  import("components/entertainment/AuthentCenter").then(module => {
    resolve(module);
  });
}
// 评论列表
const CommentList = resolve => {
  import("components/evaluating/evaluationCommentlist").then(module => {
    resolve(module);
  });
}
// 评论详情
const CommentDetail = resolve => {
  import("components/evaluating/evaluationCommentDetail").then(module => {
    resolve(module);
  });
}
Vue.use(Router);

export default new Router({
  routes: [
    /*****************登录，注册*****************/
    {
      path: "/",
      redirect: '/center'
    },
    {
      path: "/lay",
      name: "lay",
      component: Vlay,
      meta: {
        title: "系统维护"
      }
    },

    {
      path: "/404",
      name: "系统维护",
      component: Unknown,
      meta: {
        title: "系统维护"
      }
    },
    {
			path: "/mySpace/:uid/:type?",
      component: MySpace,
      meta: {
        title: "",
        requireAuth: true
      }
    },
    {
      path: '/friend',
      component: Friend,
      meta: {
        title: "我的好友"
      }
    },
    {
      path: '/inviteFriends',
      component: InviteFriends,
      meta: {
        title: "邀请好友"
      }
    },
    {
      path: "/myOrder",
      component: MyOrder,
      meta: {
        title: "我的订单",
        requireAuth: true
      }
    },
    {
			path: "/sendInfo/:type/:tid",
      component: SendInfo,
      meta: {
        title: "我的订单",
        requireAuth: true
      }
    },
    {
      path: "/login",
      component: Login,
      meta: {
        title: "登录"
      }
    },
    {
      path: "/reg",
      component: Reg,
      meta: {
        title: "注册"
      }
    },
    {
      path: "/bindMobile",
      component: BindMobile,
      meta: {
        title: "绑定手机号"
      }
    },
    {
      path: "/getCode",
      component: GetCode,
      meta: {
        title: "修改资金密码"
      }
    },
    {
      path: "/clause",
      component: Clause,
      meta: {
        title: "注册条款"
      }
    },
    {
      path: "/forgotPwd",
      component: ForgotPwd,
      meta: {
        title: "忘记密码"
      }
    },
    /*********************我的银行卡********************/
    {
      path: "/myCard",
      component: MyCard,
      meta: {
        title: "我的银行卡"
      }
    },
    /***********************福利详情**********************/
    {
      path: "/welfareDetail",
      component: (resolve) => {
        import("components/welfares/welfareDetail").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "福利详情"
      }
    },
    /***********************福利中心**********************/
    {
      path: "/welfareCenter",
      component: (resolve) => {
        import("components/welfares/welfareCenter").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "福利中心"
      }
    },
    /***********************确认订单**********************/
    {
      path: "/orderConfirm",
      component: (resolve) => {
        import("components/welfares/orderConfirm").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "确认订单"
      }
    },
    /***********************发布帖子**********************/
    {
      path: "/publishPost",
      component: (resolve) => {
        import("components/forums/publishPost").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "发布帖子"
      }
    },
    /***********************发布活动**********************/
    {
      path: "/editActivity",
      component: (resolve) => {
        import("components/forums/editActivity").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "发布活动"
      }
    },
    /***********************发布帖子成功**********************/
    {
      path: "/publishSuccess/:tid",
      component: (resolve) => {
        import("components/forums/publishSuccess").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "发布帖子成功"
      }
    },
    /***********************发布活动成功**********************/
    {
      path: "/activitySuccess/:tid",
      component: (resolve) => {
        import("components/forums/activitySuccess").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "发布帖子成功"
      }
    },
    /***********************发布悬赏成功**********************/
    {
      path: "/rewardSuccess/:tid",
      component: (resolve) => {
        import("components/forums/rewardSuccess").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "发布悬赏成功"
      }
    },
    /***********************收货地址**********************/
    {
      path: "/myAddress",
      component: (resolve) => {
        import("components/welfares/myAddress").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "收货地址"
      }
    },
    /***********************发布投票**********************/
    {
      path: "/editVote",
      component: (resolve) => {
        import("components/forums/editVote").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "发布投票"
      }
    },
    /***********************发布投票成功**********************/
    {
      path: "/voteSuccess/:tid",
      component: (resolve) => {
        import("components/forums/voteSuccess").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "发布投票成功"
      }
    },
    /***********************发布悬赏**********************/
    {
      path: "/editReward",
      component: (resolve) => {
        import("components/forums/editReward").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "发布悬赏"
      }
    },
    /***********************发布辩论**********************/
    {
      path: "/editArgue",
      component: (resolve) => {
        import("components/forums/editArgue").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "发布辩论"
      }
    },
    /***********************发布辩论成功**********************/
    {
      path: "/argueSuccess/:tid",
      component: (resolve) => {
        import("components/forums/argueSuccess").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "发布辩论成功"
      }
    },
    /***********************主页**********************/
    {
      path: "/main",
      component: Main,
      children: [


        /*********************论坛详情************************* */
        {
          path: "/forumDetail/:fid",
          component: (resolve) => {
            import("components/forums/forumDetail").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "论坛详情",
            name: 'forumDetail',
            keepAlive: true // 需要被缓存
          }
        },
        /******************我的首页****************** */
        {
          path: "/center",
          component: resolve => {
            import("components/homes/center/center").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "我的首页"
          }
        },
        {
          path: "/searchViews",
          component: resolve => {
            import("components/homes/layout/search").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "搜索框"
          }
        },
        /*********************论坛页面************************* */
        {
          path: "/forum",
          component: (resolve) => {
            import("components/homes/forum/Forum").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "论坛"
          }
        },
        {
          path: "/welfare",
          component: (resolve) => {
            import("components/homes/welfare/welfare").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "福利",
          }
        },
        /*********************活动中心************************* */
        {
          path: "/activityCenter",
          component: (resolve) => {
            import("components/activity/activityCenter").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "活动中心"
          }
        },

        /*********************活动详情************************* */
        {
          path: "/activityDetail",
          component: (resolve) => {
            import("components/activity/activityDetail").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "活动详情"
          }
        },

        /*********************开奖页面************************* */
        {
          path: "/award",
          component: (resolve) => {
            import("components/homes/award/award").then(module => {
              resolve(module);
            });
          },
          meta: {
            title: "开奖"
          }
        },
        /*********************活动详情页面************************* */
        {
          path: "/promotionDetail",
          component: PromotionDetail,
          name: "PromotionDetail",
          meta: {
            title: "活动详情"
          }
        },
        /********************客服****************** */
        {
          path: "/kefu",
          component: KeFu,
          meta: {
            title: "客服"
          }
        },
        /*******************我的页面******************** */
        {
          path: "/personal",
          component: Personal,
          meta: {
            title: "我的",
            requireAuth: true
          }
        }
      ]
    },
    /*********************个人信息************************* */
    {
      path: "/personalDetails",
      component: PersonalDetails,
      name: "PersonalDetails",
      meta: {
        title: "个人信息"
      }
    },
			{
				path: "/modifyBaseInfo/:type?",
				component: ModifyBaseInfo,
				meta: {
					title: "我的",
					requireAuth: true
				}
			},
    /*********************用户设置************************* */
    {
      path: "/userSetting",
      component: UserSetting,
      name: "UserSetting",
      meta: {
        title: "个人信息"
      }
    },

    /*********************签到页面************************* */
    {
      path: "/signin",
      component: Signin,
      name: "Signin",
      meta: {
        title: "签到页面"
      }
    },
    /*********************投注页面************************* */
    {
      path: "/betting",
      component: Betting,
      meta: {
        title: "投注记录"
      }
    },
    /*********************投注页面************************* */
    {
      path: "/zhuihao",
      component: Zhuihao,
      meta: {
        title: "投注记录"
      }
    },

    /*********************积分兑换记录************************* */
    {
      path: "/rechargeRecord",
      component: resolve => {
        import("components/personal/rechargeRecord").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "积分兑换记录"
      }
    },
    /*********************充值详情************************* */
    {
      path: "/Rechargebill",
      component: Rechargebill,
      meta: {
        title: "充值详情"
      }
    },
    /*********************提现记录************************* */
    {
      path: "/withdrawRecord",
      component: WithdrawRecord,
      meta: {
        title: "提现记录"
      }
    },
    /*********************提现详情************************* */
    {
      path: "/withdrawDetail",
      component: WithdrawDetail,
      name: "WithdrawDetail",
      meta: {
        title: "提现详情"
      }
    },
    /*********************账变记录************************* */
    {
      path: "/zbRecord",
      component: ZbRecord,
      meta: {
        title: "账变记录"
      }
    },
    /*********************消息中心************************* */
    {
      path: "/msgCenter",
      component: MsgCenter,
      meta: {
        title: "我的消息"
      }
    },
    /*********************我的提醒************************* */
    {
      path: "/remind",
      component: Remind,
      meta: {
        title: "我的提醒"
      }
    },
    /*********************我的收藏************************* */
    {
      path: "/collection",
      component: Collection,
      meta: {
        title: "我的收藏"
      }
    },
    /*********************我的收藏************************* */
    {
      path: "/bbs/:id",
      component: Bbs,
      meta: {
        title: "我的帖子"
      }
    },


    /*********************修改密码************************* */
    {
      path: "/modifyPwd",
      component: ModifyPwd,
      meta: {
        title: "修改密码"
      }
    },

    /*********************绑定手机号************************* */
    // {
    //   path: "/bindMobile",
    //   component: BindMobile,
    //   meta: {
    //     title: "绑定手机号"
    //   }
    // },
    {
      path: "/setPwd/",
      name: "setPwd",
      component: SetPwd,
      meta: {
        title: "设置资金密码"
      }
    },
    /*********************入驻页面************************* */
    {
      path: "/SettledInfo",
      name: "SettledInfo",
      component: SettledInfo,
      meta: {
        title: "入驻合作"
      }
    },
    /*********************评测中心************************* */
    {
      path: "/evaluating",
      name: "evaluating",
      component: Evaluating,
      meta: {
        title: "评测中心"
      }
    },
    /*********************娱乐城************************* */
    {
      path: "/entertainment",
      name: "entertainment",
      component: Entertainment,
      meta: {
        title: "娱乐城详情"
      }
    },
    /*********************认证中心************************* */
    {
      path: "/authent",
      name: "authent",
      component: Authent,
      meta: {
        title: "认证中心"
      }
    },
    /*********************评测中心-评论列表************************* */
    {
      path: "/evaluating-commentList",
      name: "commentList",
      component: CommentList,
      meta: {
        title: "评论中心"
      }
    },
    /*********************评测中心-评论列表-评论详情************************* */
    {
      path: "/evaluating-commentDetail",
      name: "commentDetail",
      component: CommentDetail,
      meta: {
        title: "评论详情"
      }
    },
    /*********************博天堂担保************************* */
    {
      path: "/guarantee",
      component: (resolve) => {
        import("components/homes/layout/guarantee").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "博天堂担保"
      }
    },
    /*********************关于博天堂************************* */
    {
      path: "/aboutBOtiantang",
      component: (resolve) => {
        import("components/homes/layout/aboutBOtiantang").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "关于博天堂"
      }
    },
    /***********************发布辩论成功**********************/
    {
      path: "/myCardDetail",
      component: (resolve) => {
        import("components/index/withdrawCash/myCardDetail").then(module => {
          resolve(module);
        });
      },
      meta: {
        title: "每日任务详情"
      }
    },
  ]
});

