/**
 * Created by easterCat on 2019/4/9.
 */

/**
 * 将秒格式为 time:minute:second
 * @param value 需要格式的时间
 * @param sep 中间的间隔符号
 * @returns {string[]}
 */

export function formatSeconds(value, sep) {
  var secondTime = parseInt(value); // 秒
  var minuteTime = 0; // 分
  var hourTime = 0; // 小时
  var result;
  sep = sep || ':';

  if (secondTime > 60) {
    //如果秒数大于60，将秒数转换成整数
    //获取分钟，除以60取整数，得到整数分钟
    minuteTime = parseInt(secondTime / 60);
    //获取秒数，秒数取佘，得到整数秒数
    secondTime = parseInt(secondTime % 60);
    //如果分钟大于60，将分钟转换成小时
    if (minuteTime > 60) {
      //获取小时，获取分钟除以60，得到整数小时
      hourTime = parseInt(minuteTime / 60);
      //获取小时后取佘的分，获取分钟除以60取佘的分
      minuteTime = parseInt(minuteTime % 60);
    }
  }

  if (secondTime && secondTime > 0) {
    result = `${_addZero(parseInt(secondTime))}`;
  } else {
    result = "00";
  }

  if (minuteTime && minuteTime > 0) {
    result = `${_addZero(parseInt(minuteTime))}` + sep + result;
  } else {
    result = "00" + sep + result;
  }

  if (hourTime && hourTime > 0) {
    result = `${_addZero(parseInt(hourTime))}` + sep + result;
  } else {
    result = "00" + sep + result;
  }

  return {
    arr: result.split(sep),
    str: result
  };
}


function _addZero(num) {
  return num >= 10 ? num : `0${num}`;
}
