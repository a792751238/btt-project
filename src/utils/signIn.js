let SignIn = new class {
  constructor() {
    this.days = {
      0: "星期日",
      1: "星期一",
      2: "星期二",
      3: "星期三",
      4: "星期四",
      5: "星期五",
      6: "星期六"
    };
    this.myMonths = [[], [], [], [], [], [], []];
    this.signedDays = [];
    this.year = "";
    this.month = "";
    this.continueDay = "";
    this.signing = false;

    this.singCallback = () => {
    };

    this._createMyMonths = this._createMyMonths.bind(this);
    this._createMonthsHeader = this._createMonthsHeader.bind(this);
    this._createMonthsBody = this._createMonthsBody.bind(this);
    this._drawTableInHtml = this._drawTableInHtml.bind(this);
  }

  render(options) {
    const {year, month, con, signed, continueDay, signing, singCallback} = options;
    this.signedDays = signed;
    this.year = year;
    this.month = month;
    this.continueDay = continueDay;
    this.signing = signing;
    this.singCallback = singCallback;
    let content = document.getElementById(con);
    this._createMyMonths(year, month);
    content.innerHTML = this._drawTableInHtml();

    document.getElementById("signBtn").addEventListener("click", this.singCallback);
  }

  //生成当月的星期数组
  _createMyMonths(year, month) {
    let dayDate = new Date(year, month - 1, 1);
    let monthDate = new Date(year, month, 0);
    let firstDayInMonth = dayDate.getDay();
    let allDaysInMonth = monthDate.getDate();

    this._createMonthsHeader();
    this._createMonthsBody(firstDayInMonth, allDaysInMonth);
  }

  _createMonthsHeader() {
    this.myMonths[0] = ["日", "一", "二", "三", "四", "五", "六"];
  }

  _createMonthsBody(firstDay, allDays) {
    let num = 1;
    let d, row, col;

    for (d = firstDay; d < 7; d++) {
      this.myMonths[1][d] = num;
      num++;
    }

    for (row = 2; row < 7; row++) {
      for (col = 0; col < 7; col++) {
        if (num <= allDays) {
          this.myMonths[row][col] = num;
          num++;
        }
      }
    }
  }

  _drawTableInHtml() {
    let htmls = [];
    htmls.push("<div class='signIn-main'>");
    htmls.push("<div class='sign-btn-con'><span class='continue'>您已连续签到<span class='red'>" + this.continueDay + "</span>天</span><button id='signBtn' class='" + (this.signing ? "active" : "") + "'>" + (this.signing ? "签到" : "已签到") + "</button>");
    htmls.push("</div>");
    htmls.push("<div class='year-month-span'>" + this.year + "年" + this.month + "月" + "</div>");
    htmls.push("<div class='sign' id='sign_cal'>");
    htmls.push("<table class='table'  border='0' cellpadding='0' cellspacing='0'>");
    htmls.push("<thead><tr>");
    htmls.push("<th>" + this.myMonths[0][0] + "</th>");
    htmls.push("<th>" + this.myMonths[0][1] + "</th>");
    htmls.push("<th>" + this.myMonths[0][2] + "</th>");
    htmls.push("<th>" + this.myMonths[0][3] + "</th>");
    htmls.push("<th>" + this.myMonths[0][4] + "</th>");
    htmls.push("<th>" + this.myMonths[0][5] + "</th>");
    htmls.push("<th>" + this.myMonths[0][6] + "</th>");
    htmls.push("</tr></thead>");
    let d, w;

    htmls.push("<tbody>");
    for (w = 1; w < 7; w++) {
      htmls.push("<tr>");
      for (d = 0; d < 7; d++) {
        let ifHasSigned = !isNaN(this.myMonths[w][d]) ? this.signedDays.includes(this.myMonths[w][d]) : false;
        if (ifHasSigned) {
          htmls.push("<td class='signed'><span>" + (!isNaN(this.myMonths[w][d]) ? this.myMonths[w][d] : " ") + "</span></td>");
        } else {
          htmls.push("<td><span>" + (!isNaN(this.myMonths[w][d]) ? this.myMonths[w][d] : " ") + "</span></td>");
        }
      }
      htmls.push("</tr>");
    }
    htmls.push("</tbody>");
    htmls.push("</table>");
    htmls.push("</div>");
    htmls.push("</div>");
    return htmls.join("");
  }
}();

export default SignIn;
