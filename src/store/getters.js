// 对store做映射文件 使用getter取store中的数据
export default {
  download: (state) => {
    return state.siteBaseInfo.download;
  },
  initData: state => state.initData,
	userInfo: state	=> state.userInfo,
  slide_menu: state => {
    return state.homeMenu.slide_menu
  },
  ads_above_list: state => {
    return state.homeMenu.ads_above_list
  },
  slide_nav_menu: state => {
    return state.homeMenu.slide_nav_menu
  },
  activity_list: state => {
    return state.homeMenu.activity_list
  },
  video_url: state => {
    return state.homeMenu.video_url
  },
  auth_menu1: state => {
    let arr = [].concat(state.homeMenu.auth_menu);
    return arr.filter(item => item.groupid === "1");
  },
  auth_menu2: state => {
    let arr = [].concat(state.homeMenu.auth_menu);
    return arr.filter(item => item.groupid === "2");
  },
  auth_menu3: state => {
    let arr = [].concat(state.homeMenu.auth_menu);
    return arr.filter(item => item.groupid === "3");
  },
  auth_menu4: state => {
    let arr = [].concat(state.homeMenu.auth_menu);
    return arr.filter(item => item.groupid === "4");
  },
}
