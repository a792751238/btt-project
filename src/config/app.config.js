let baseUrl = concat_url();
let baseUrl2 = concat_url2();
let platform = "博天堂";
let platform_prefix = "";
let open_valid_user = "false"; //是否开启有效用户,美高梅关闭,东方汇/趣多开启

module.exports = {
    baseUrl,
    baseUrl2,
    platform,
    platform_prefix,
    open_valid_user,
};

function concat_url() {
    return `${location.protocol}//${location.host}`;
}

function concat_url2() {
    let url;
    let home_invite_uri = sessionStorage.getItem('home_invite_uri');
    if (home_invite_uri && home_invite_uri.length > 0) {
        url = `${home_invite_uri}`;
    } else {
        url = `${location.protocol}//${location.host}`;
    }
    return url;
}