import {fetch} from "./config";

// 登陆
export function login_by_username_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=login`, params, "post");
}

// 登陆
export function login_by_mobile_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=mobilelogin`, params, "post");
}

//
export function get_mobile_code_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=sendmobilecode`, params, "post");
}

// 登出
export function logout_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=logout`, params, "get");
}

//注册设置
export function regSet_api(params) {
  return fetch(`/api/user/register_setting`, params, "post");
}

// 注册
export function reg_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=register`, params, "post");
}

// 手机号注册
export function register_mobile_api(params) {
  return fetch(`/api/user/register_mobile`, params, "post");
}

// 图片验证码
export function image_api(params) {
  return fetch(`/api/other/vcode_image`, params, "post");
}

// 手机验证码
export function mobile_api(params) {
  return fetch(`/api/other/vcode_mobile`, params, "post");
}


// 手机验证码
export function code_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=getcode`, params, "get");
}

// 设置用户名
export function username_api(params) {
  return fetch(`/api/mine/username`, params, "post");
}

// 绑定微信
export function wxbind_api(params) {
  return fetch(`/api/other/wxbind`, params, "post");
}

// 奖励阅读
export function award_read_api(params) {
  return fetch(`/api/mine/award_read`, params, "post");
}


// 设置用户名
export function userinfo_api(params) {
  return fetch(`/api/user/userinfo_change`, params, "post");
}

// 登陆密码修改
export function pwdChange_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=homeprofile`, params, "post");
}

// 登陆密码找回
export function pwdGet_api(params) {
  return fetch(`/api/user/password_find`, params, "post");
}

// 资金密码修改
export function moneypwdChange_api(params) {
  return fetch(`/api/user/fundpwd_change`, params, "post");
}

// 资金密码找回
export function moneypwdGet_api(params) {
  return fetch(`/api/user/fundpwd_find`, params, "post");
}

// 资金密码检查
export function moneypwdCheck_api(params) {
  return fetch(`/api/user/fundpwd_check`, params, "post");
}

// 绑定手机号
export function mobile_bind_api(params) {
  return fetch(`/api/user/mobile_bind`, params, "post");
}

// 提现首页信息 并获取是否设置支付密码
export function withdraw_api(params) {
  return fetch(`/api/withdraw/info`, params, "post");
}

// 申请提现
export function withdrawApply_api(params) {
  return fetch(`/api/withdraw/apply`, params, "post");
}

// 申请提现
export function bankcard_bank_api(params) {
  return fetch(`/api/withdraw/bankcard_bank`, params, "post");
}

// 提现记录
export function withdraw_log_api(params) {
  return fetch(`/api/mine/withdraw`, params, "post");
}

// 游戏记录
export function game_record_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=viewthread&tid=20261`, params, "get");
}

// 添加银行卡
export function addCard_api(params) {
  return fetch(`/api/withdraw/bankcard_add`, params, "post");
}

// 设置默认银行卡
export function bankcard_default_api(params) {
  return fetch(`/api/withdraw/bankcard_default`, params, "post");
}

// 银行卡列表
export function cardList_api(params) {
  return fetch(`/api/withdraw/bankcard_list`, params, "post");
}

// 博币兑换彩金 or 彩金兑换博币
export function exchange_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=exchange`, params, "post");
}

// 线上充值二维码
export function onlineQrcodeApi_api(params) {
  return fetch(`/api/recharge/online_qrcode`, params, "post");
}

// 积分兑换记录
export function get_credit_record(params) {
  return fetch(`/api/mobile/index.php?version=4&module=creditlog`, params, "get");
}

// 账变记录
export function scoreLog_api(params) {
  return fetch(`/api/mine/change`, params, "post");
}

// 消息列表
export function msgList_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=mypm`, params, 'get');
}

// 消息详情
export function notice_msg_api(params) {
  return fetch(`/api/mobile/index.php?version=5&module=chat`, params, 'get');
}

// 发送消息
export function send_msg_api(params) {
  return fetch(`/api/mobile/index.php?version=5&module=chatsendpm`, params, 'post');
}

// 发送消息
export function get_public_msg_api(params) {
  return fetch(`/api/mobile/index.php?version=5&module=announcepm`, params, 'get');
}

// 我的首页
export function home_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=forumindex`, params, "get");
}

// 上传头像
export function avatarApi_api(params) {
  return fetch(`/api/mobile/index.php?version=5&module=uploadavatar`, params, "post");
}

// 个人中心
export function index_api(params) {
  return fetch(`/api/other/index`, params, "post");
}

// 活动列表
export function activity_list_api(params) {
  return fetch(`/api/activity/home`, params, "post");
}

// 活动详情
export function activity_detail_api(params) {
  return fetch(`/api/activity/detail`, params, "post");
}

// 游戏地址
export function game_api(params) {
  return fetch(`/api/game/launch`, params, "post");
}

// 游戏余额
export function game_balance_api(params) {
  return fetch(`/api/game/balance`, params, "post");
}

// 余额
export function my_balance_api(params) {
  return fetch(`/api/mine/my_balance`, params, "post");
}

// 游戏金额转账
export function game_transfer_api(params) {
  return fetch(`/api/game/transfer`, params, "post");
}

// 游戏列表
export function game_transfer_list_api(params) {
  return fetch(`/api/game/transfer_list`, params, "post");
}

//购彩大厅接口
export function lotteryLobbyInfo_api(params) {
  return fetch(`/api/lottery/lotteryLobbyInfo`, params, "post");
}

//彩票首页接口
export function lotteryList_api(params) {
  return fetch(`/api/lottery/index`, params, "post");
}

//彩票首页接口
export function getLotteryOrderInfo_api(params) {
  return fetch(`/api/lottery/getLotteryOrderInfo`, params, "post");
}

//彩票投注接口
export function lotteryBetting_api(params) {
  return fetch(`/api/lottery/lotteryBetting`, params, "post");
}

//彩票聊天信息接口
export function lotteryRoomList_api(params) {
  return fetch(`/api/lottery/lotteryRoomList`, params, "post");
}

//彩票聊天室广告信息接口
export function getchatRoomAdInfo_api(params) {
  return fetch(`/api/lottery/getchatRoomAdInfo`, params, "post");
}

//彩票弹框接口
export function lotteryRoomDetail_api(params) {
  return fetch(`/api/lottery/lotteryRoomDetail`, params, "post");
}

// 开奖记录列表
export function lotteryAwardRecordList_api(params) {
  return fetch(`/api/award/lotteryAwardRecordList`, params, "post");
}

// 开奖结果接口
export function getLotteryAwardResult_api(params) {
  return fetch(`/api/award/getLotteryAwardResult`, params, "post");
}

//快三走势接口
export function lotteryAwardTrend_api(params) {
  return fetch(`/api/award/lotteryAwardTrend`, params, "post");
}

//投注撤单接口
export function cancelOrder_api(params) {
  return fetch(`/api/lottery/cancelOrder`, params, "post");
}

//订单记录列表接口
export function bettingRecord_api(params) {
  return fetch(`/api/lottery/bettingRecord`, params, "post");
}

//订单下拉彩种信息
export function lotteryType_api(params) {
  return fetch(`/api/lottery/lotteryList`, params, "post");
}

//订单详情接口
export function bettingDetail_api(params) {
  return fetch(`/api/lottery/bettingDetail`, params, "post");
}

//彩票类型玩法展示页
export function lotteryDetail_api(params) {
  return fetch(`/api/lottery/lotteryDetail`, params, "post");
}

// 彩票规则
export function lotteryExplainShow_api(params) {
  return fetch(`/api/lottery/lotteryExplainShow`, params, "post");
}

// 彩票规则
export function lotteryPlayIntroduce_api(params) {
  return fetch(`/api/lottery/lotteryPlayIntroduce`, params, "post");
}

//开户中心推广的列表
export function invitation_list_api(params) {
  return fetch(`/api/agent/team/invitation_list`, params, "post");
}

export function remove_invitation_list_api(params) {
  return fetch(`/api/agent/team/invitation_delete`, params, "post");
}

export function create_user_api(params) {
  return fetch(`/api/agent/team/invitation_register`, params, "post");
}

export function create_link_api(params) {
  return fetch(`/api/agent/team/invitation_create`, params, "post");
}

export function get_default_invitation(params) {
  return fetch(`/api/agent/team/invitation_default`, params, "post");
}

export function get_my_salary(params) {
  return fetch(`/api/agent/salary/record_mine`, params, "post");
}

export function get_under_salary(params) {
  return fetch(`/api/agent/salary/record_child`, params, "post");
}

export function get_contract_list(params) {
  return fetch(`/api/agent/salary/contract_list`, params, "post");
}

export function sign_cur_user(params) {
  return fetch(`/api/agent/salary/contract_submit`, params, "post");
}

export function get_team_moneu(params) {
  return fetch(`/api/agent/report/lottery_team`, params, "post");
}

export function get_my_devidend(params) {
  return fetch(`/api/agent/profit/record_mine`, params, "post");
}

export function get_under_devidend(params) {
  return fetch(`/api/agent/profit/record_child`, params, "post");
}

export function get_profit_contract_list(params) {
  return fetch(`/api/agent/profit/contract_list`, params, "post");
}

export function get_member_list(params) {
  return fetch(`/api/agent/team/child_member`, params, "post");
}

export function up_to_agent(params) {
  return fetch(`/api/agent/team/child_upgrade`, params, "post");
}

//提交转账数据
export function submit_tranfer(params) {
  return fetch(`/api/agent/team/agent_transfer_submit`, params, "post");
}

//提交调点数据
export function submit_adjust(params) {
  return fetch(`/api/agent/team/child_audjst_submit`, params, "post");
}

// 获取团队概况
export function get_team_situation(params) {
  return fetch(`/api/agent/team/situation`, params, "post");
}

//获取我的返水
export function get_my_water(params) {
  return fetch(`/api/agent/water/mine`, params, "post");
}

//获取我的返水根据我的日期
export function get_my_water_daily(params) {
  return fetch(`/api/agent/water/daily`, params, "post");
}

//获取返水报表
export function get_team_detail(params) {
  return fetch(`/api/agent/water/detail`, params, "post");
}

//获取信息报表
export function get_message_list(params) {
  return fetch(`/api/other/message_list`, params, "post");
}

// 消息已阅读
export function announcement_read_api(params) {
  return fetch(`/api/mine/announcement_read`, params, "post");
}

//已读过信息报表
export function read_message_list(params) {
  return fetch(`/api/other/message_read`, params, "post");
}

//代理中心首页
export function get_agent_team_home(params) {
  return fetch(`/api/agent/team/home`, params, "post");
}

//代理中心首页 我的返水比例
export function get_agent_water_ratio(params) {
  return fetch(`/api/agent/water/ratio`, params, "post");
}

//代理中心下级-投注记录
export function get_agent_team_child_betting_v2(params) {
  return fetch(`/api/agent/team/child_betting_v2`, params, "post");
}

//代理中心下级-充值记录
export function get_agent_team_child_recharge(params) {
  return fetch(`/api/agent/team/child_recharge`, params, "post");
}

//代理中心下级-提现记录
export function get_agent_team_child_withdraw(params) {
  return fetch(`/api/agent/team/child_withdraw`, params, "post");
}

//代理中心下级-成员管理
export function get_agent_team_child_member_v2(params) {
  return fetch(`/api/agent/team/child_member_v2`, params, "post");
}

//代理中心下级-成员管理头部
export function get_agent_team_topbar(params) {
  return fetch(`/api/agent/team/topbar`, params, "post");
}

//代理中心下级-成员管理头部
export function get_agent_team_child_info(params) {
  return fetch(`/api/agent/team/child_info`, params, "post");
}

export function get_register_number(params) {
  return fetch(`/api/agent/team/register_number`, params, "post");
}

export function get_statistics_rank(params) {
  return fetch(`/api/agent/team/statistics_rank`, params, "post");
}

export function get_statistics(params) {
  return fetch(`/api/agent/team/statistics`, params, "post");
}

export function get_child_betting(params) {
  return fetch(`/api/agent/team/child_betting`, params, "post");
}

export function get_child_recharge(params) {
  return fetch(`/api/agent/team/child_recharge`, params, "post");
}

export function get_child_withdraw(params) {
  return fetch(`/api/agent/team/child_withdraw`, params, "post");
}

export function get_sign_show(params) {
  return fetch(`/api/mobile/index.php?version=5&module=checkin`, params, "get");
}

export function post_signed(params) {
  return fetch(`/api/user/signin_submit`, params, "post");
}

//上下级聊天-成员列表
export function level_list_api(params) {
  return fetch(`/api/chat/level_list`, params, "post");
}

//上下级聊天-搜索
export function chat_seach_api(params) {
  return fetch(`/api/chat/seach`, params, "post");
}

//上下级聊天-未读数量
export function not_read_count_api(params) {
  return fetch(`/api/chat/not_read_count`, params, "post");
}

//上下级聊天-未读消息列表
export function not_read_list_api(params) {
  return fetch(`/api/chat/not_read_list`, params, "post");
}

//上下级聊天-消息发送
export function send_message_api(params) {
  return fetch(`/api/chat/send_message`, params, "post");
}

//上下级聊天-聊天数据
export function chat_list_api(params) {
  return fetch(`/api/chat/chat_list`, params, "post");
}

export function get_audio_list(params) {
  return fetch(`/api/mine/voice_tip`, params, "post");
}

export function get_mask_detail(params) {
  return fetch(`/api/mine/mask_detail`, params, "post");
}

//清空未读
export function chat_notice_read_api(params) {
  return fetch(`/api/chat/notice_read`, params, "post");
}


//充值订单-取消
export function get_recharge_cancel(params) {
  return fetch('/api/recharge/cancel', params, "post");
}

//充值订单-重新支付
export function get_recharge_repay(params) {
  return fetch('/api/recharge/repay', params, "post");
}

export function site_common_set(params) {
  return fetch(`/api/other/common`, params, 'post')
}

//start added by ranka

//好彩左侧彩票列表接口
export function lotteryMenu_api(params) {
  return fetch(`/api/lottery/menu`, params, "post");
}

//彩票首页接口
export function lotteryHome_api(params) {
  return fetch(`/api/lottery/home`, params, "post");
}

//彩票首页开奖接口
export function get_award_list(params) {
  return fetch('/api/lottery/award_result', params, "post");
}

//开户管理API start

export function get_createAccount_list(params) {
  return fetch('/api/agent/opening/link_list', params, "post");
}

export function add_createAccount_list_item(params) {
  return fetch('/api/agent/opening/link_add', params, "post");
}


export function get_home_menu(params) {
  return fetch('/api/mobile/index.php?version=4&module=getmenu', params, "get");
}

export function get_home_forums(params) {
  return fetch('/api/mobile/index.php?version=5&module=forumindex', params, "get");
}

export function get_forum_posts(params, fid, page) {
  return fetch(`/api/mobile/index.php?version=4&module=forumdisplay&fid=${fid}&page=${page}`, params, "get");
}

export function get_forum_posts_hot(params, fid, page = 1) {
  return fetch(`/api/mobile/index.php?version=4&module=forumdisplay&filter=heat&orderby=heats&fid=${fid}&page=${page}`, params, "get");
}

export function get_forum_posts_better(params, fid, page = 1) {
  return fetch(`/api/mobile/index.php?version=4&module=forumdisplay&filter=digest&digest=1&orderby=lastpost&fid=${fid}&page=${page}`, params, "get");
}

export function get_forum_posts_new(params, fid, page = 1) {
  return fetch(`/api/mobile/index.php?version=4&module=forumdisplay&filter=lastpost&orderby=lastpost&fid=${fid}&page=${page}`, params, "get");
}

// 娱乐城详情
export function get_evaluating_center(params, certid, page) {
  return fetch(`/api/mobile/index.php?version=5&module=certification&certid=${certid}`, params, "get");
}

// 我的 信息
export function get_user_info(params) {
  return fetch(`/api/mobile/index.php?version=5&module=usercenter`, params, "get");
}

// 发布接口
export function publish_info(params, fid) {
  return fetch(`/api/mobile/index.php?version=4&module=newthread&fid=${fid}`, params, "post");
}

// 发布接口
export function get_froum_info(params, tid) {
  return fetch(`/api/mobile/index.php?version=5&module=viewthread&tid=${tid}`, params, "get");
}

export function get_goods(params, page) {
  return fetch(`/api/mobile/index.php?version=5&module=goodsdata&page=${page}`, params, "get");
}

export function get_goods_detail(params, tid, page) {
  return fetch(`/api/mobile/index.php?version=5&module=goodsdatabytid&tid=${tid}&page=${page}`, params, "get");
}

export function modify_address(params) {
  return fetch(`/api/mobile/index.php?version=5&module=changeaddr`, params, "post");
}

export function get_address(params) {
  return fetch(`/api/mobile/index.php?version=5&module=getaddr`, params, "get");
}

export function buy(params) {
  return fetch(`/api/mobile/index.php?version=5&module=placeanorder`, params, "post");
}

//帖子评论接口
export function comment(params, tid, fid) {
  return fetch(`/api/mobile/index.php?version=4&module=sendreply&tid=${tid}&fid=${fid}`, params, "post");
}

//辩论帖子加入正反方接口
export function comment_join(params, tid, fid, stand) {
  return fetch(`/api/mobile/index.php?version=4&module=sendreply&tid=${tid}&fid=${fid}&usesig=1&stand=${stand}`, params, "post");
}

// 我的提醒里面的 我的粉丝
export function get_my_follower(params) {
  return fetch(`/api/mobile/index.php?version=5&module=myfollower`, params, "get");
}

// 我的提醒 里面的 我的帖子、坛友互动、系统提醒
export function get_remind(params) {
  return fetch(`/api/mobile/index.php?version=4&module=mynotelist`, params, "get");
}

// 认证中心列表接口
export function get_anthentCenter(params) {
  return fetch(`/api/mobile/index.php?version=5&module=certificateauthority`, params, "get");
}

// 获取认证分类
export function get_anthent_select(params) {
  return fetch(`/api/mobile/index.php?version=5&module=getcategory`, params, "get");
}

// 评测中心
export function get_comment(params) {
  return fetch(`/api/mobile/index.php?version=5&module=evaluation`, params, "get");
}

// 认证中心详情
export function ajax_upload(params, fid) {
  return fetch(`/api/mobile/index.php?version=4&module=forumupload&operation=upload&type=image&simple=2&infloat=yes&inajax=yes&fid=${fid}`, params, "POST");
}

// 我的帖子
export function get_bbs(params) {
  return fetch(`/api/mobile/index.php?version=4&module=getthreadbyuid&do=thread&view=me&from=space`, params, "get");
}

// 我的资料
export function get_my_id_info(params) {
  return fetch(`/api/mobile/index.php?version=4&module=getprofilebyuid`, params, "get");
}

// 帖子评论
export function add_commnt(params) {
  return fetch(`/api/mobile/index.php?version=4&module=sendreply&tid=1&fid=2`, params, 'post')
}

// 帖子评论
export function get_activity(params) {
  return fetch(`/api/mobile/index.php?version=5&module=specialoffer`, params, 'get')
}// 帖子评论
export function get_activity_detail(params, proid) {
  return fetch(`/api/mobile/index.php?version=5&module=specialofferbyid&proid=${proid}`, params, 'get')
}

export function get_tasks(params) {
  return fetch(`/api/mobile/index.php?version=5&module=usertask`, params, 'get')
}

export function get_tasks_detail(params, tid) {
  return fetch(`/api/mobile/index.php?version=5&module=usertaskview&id=${tid}`, params, 'post')
}

export function add_recommend(params, tid) {
  return fetch(`/api/mobile/index.php?version=4&module=recommend&do=add&tid=${tid}`, params, 'get')
}

export function delete_recommend(params, tid) {
  return fetch(`/api/mobile/index.php?version=4&module=recommend&do=delete&tid=${tid}`, params, 'get')
}

//板块关注
export function favforum(params, id) {
  return fetch(`/api/mobile/index.php?version=4&module=favforum&id=${id}`, params, 'post')
}

//判断板块是否关注
export function checkfavforum(params, forumid) {
  return fetch(`/api/mobile/index.php?version=4&module=checkfavforum&forumid=${forumid}`, params, 'post')
}

//判断板块是否关注
export function delete_favforum(params, favid, type) {
  return fetch(`/api/mobile/index.php?version=4&module=favforum&op=delete&favid=${favid}&type=${type}`, params, 'post')
}

// 生成新的邀请码
export function create_QRcode_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=invitefriend`, params, 'post')
}

// 用户收藏
export function get_collcetion_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=myfavforum`, params, 'get')
}

export function debatevote(params, tid, stand) {
  return fetch(`/api/mobile/index.php?version=5&module=debatevote&tid=${tid}&stand=${stand}`, params, 'get')
}

// 评测中心-评论列表
export function get_comment_list(params, certid) {
  return fetch(`/api/mobile/index.php?version=5&module=evaluationcommentlist&certid=${certid}`, params, 'get')
}

// 评论中心 - 评论列表 - 评论详情
export function get_comment_detail(params, reviewid) {
  return fetch(`/api/mobile/index.php?version=5&module=evaluationcommentbyid&reviewid=${reviewid}`, params, 'get')
}


// 评论中心 - 评论列表 - 评论详情
export function vote_api(params) {
  return fetch(`/api/mobile/index.php?version=5&module=vote`, params, 'post')
}

// 赞赏功能
export function reward_api(params) {
  return fetch(`/api/mobile/index.php?version=5&module=reward`, params, 'post')
}

// 订单列表
export function get_orderList_api(params) {
  return fetch(`/api/mobile/index.php?version=5&module=getbuylist`, params, 'get')
}

// 卖出的和我发布的
export function get_sellOutAndReleaseList_api(params) {
  return fetch(`/api/mobile/index.php?version=5&module=soldorder`, params, 'post')
}

// 收货地址
export function get_address_api(params) {
  return fetch(`/api/mobile/index.php?version=5&module=getaddr`, params, 'get')
}


// 赞赏功能
export function favthread_api(params, id) {
  return fetch(`/api/mobile/index.php?version=4&module=favthread&id=${id}`, params, 'post')
}

// 搜索页面信息
export function searchpage_api(params) {
  return fetch(`/api/mobile/index.php?version=5&module=searchpage`, params, 'get')
}

// 搜索接口1
export function search_one_api(params, id) {
  return fetch(`/api/mobile/index.php?version=4&module=searchforum&mobile=2&kw=${id}`, params, 'get')
}

// 搜索接口2
export function search_two_api(params) {
  return fetch(`/api/mobile/index.php?version=4&module=searchforum`, params, 'get')
}

// 上传ip
export function get_ip_api(params) {
  return fetch(`/api/mobile/index.php?version=5&module=ip`, params, 'get')
}

// 获取公告
export function get_notice_api(params) {
  return fetch('/api/mobile/index.php?version=5&module=getannounce', params, 'get');
}

// 我的好友
export function get_myFriends_api(params) {
	return fetch('/api/mobile/index.php?version=5&module=myfriends', params, 'get');
}

// 关注好友
export function add_friend_api(params) {
	return fetch('/api/mobile/index.php?version=5&module=followfriend', params, 'get');
}

// 更换备注和删除哈有
export function modify_friend_api(params, op, uid) {
	if(uid) { 
		uid	= `&uid=${uid}`; 
	} else {
		uid	= '';
	}
	return fetch(`/api/mobile/index.php?version=5&module=friendoperation&op=${op}${uid}`, params, 'post');
}

// 发货信息
export function get_sendInfo_api(params) {
	return fetch(`/api/mobile/index.php?version=5&module=soldorderbyid`, params, 'get');
}
export function add_sendInfo_api(params) {
	return fetch(`/api/mobile/index.php?version=5&module=sendout`, params, 'post');
}

// 修改基本资料
export function add_base_api(params) {
	return fetch(`/api/mobile/index.php?version=4&module=homeprofile&op=base`, params, 'post');
}
// 修改联方式
export function add_contact_api(params) {
	return fetch(`/api/mobile/index.php?version=4&module=homeprofile&op=contact`, params, 'post');
}
// 修改教育情况
export function add_education_api(params) {
	return fetch(`/api/mobile/index.php?version=4&module=homeprofile&op=edu`, params, 'post');
}
// 修改工作情况
export function add_work_api(params) {
	return fetch(`/api/mobile/index.php?version=4&module=homeprofile&op=work`, params, 'post');
}
// 修改工人信息情况
export function add_info_api(params) {
	return fetch(`/api/mobile/index.php?version=4&module=homeprofile&op=info`, params, 'post');
}
// 参加活动 退出活动
export function modify_signUpActivicy_api (params) {
	let {fid, tid, pid} =params;
	return fetch(`/api/mobile/index.php?version=5&module=activityapplies&fid=${ fid }&tid=${ tid}&pid=${ pid}`, params, 'post');
}

// 删除发帖时候里面的图片
export function remove_img_api (params) {
	console.log(params)
	return fetch(`/api/mobile/index.php?version=5&module=ajaxdeleteimg`, params, 'get');
}



